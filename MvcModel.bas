
#Include Once "ModelBase.bas"

Type MvcModel Extends ModelBase
    Declare Constructor()

    As String SomeString = ""
    as Integer SomeInteger = 0
    As Boolean SomeBoolean = False
    
    Private:
     bForceNotify As Boolean = False
    
end type
